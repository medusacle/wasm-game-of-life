#!/bin/bash
set -ex
SRC="${PWD}"
OUT="${PWD}/dist"

cargo build --target wasm32-unknown-unknown --release

mkdir -p ${OUT}
wasm-bindgen ./target/wasm32-unknown-unknown/release/wasm_game_of_life.wasm --no-typescript --no-modules --out-dir ${OUT}
cp ${SRC}/www/index.html ${OUT}
