# Conway's Game of Life

Implementation of Conway's game of life using Rust and WebAssembly, based on
the [tutorial](https://rustwasm.github.io/book/introduction.html).

The main difference with the tutorial is that all the parts have been
implemented in Rust, including canvas rendering and event handlers. The only
hand-written javascript left is:

```js
const { main } = wasm_bindgen;

var eventloop;

function run() {
    eventloop = main(
        document.getElementById("game-of-life-canvas"),
        document.getElementById("play-pause"),
        document.getElementById("randomize"),
        document.getElementById("omnicide"));
}

wasm_bindgen('./wasm_game_of_life_bg.wasm').then(run);
```

HTML elements are passed in, as I found it useful to not hardcode the ids inside
the wasm (though they easily could be). The returned `eventloop` handle holds
on to closures &c.

This is my own experiment to see if a web game can be completely implemented in
Rust and `web-sys`.

## Dependencies

This requires the Rust toolchain for WebAssembly (stable will work as of Rust
1.31), to install this use the following one-time setup:

```
rustup target add wasm32-unknown-unknown
cargo install wasm-bindgen-cli
```

## Running and building

To build everything and start a webserver on `0.0.0.0:8080`, run:
```
./run.sh
```

There's also `./build.sh` that will build a distribution directory in `dist`,
for deploying to a separate web server.

