/*** EVENT HANDLING ***/
use std::cell::RefCell;
use std::rc::Rc;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

use crate::renderer::Renderer;
use crate::universe::Universe;

pub struct RenderLoop {
    window: web_sys::Window,
    playpause_button: web_sys::HtmlElement,
    universe: Rc<RefCell<Universe>>,
    renderer: Rc<RefCell<Renderer>>,
    animation_id: Option<i32>,
    pub closure: Option<Closure<dyn Fn(f64)>>,
}

impl RenderLoop {
    pub fn new(
        window: web_sys::Window,
        playpause_button: web_sys::HtmlElement,
        universe: Rc<RefCell<Universe>>,
        renderer: Rc<RefCell<Renderer>>,
    ) -> RenderLoop {
        RenderLoop {
            window: window,
            playpause_button: playpause_button,
            universe: universe,
            renderer: renderer,
            animation_id: None,
            closure: None,
        }
    }

    pub fn render_loop(&mut self, _time: f64) {
        self.universe.borrow_mut().tick();
        self.renderer.borrow().draw();

        self.animation_id = if let Some(ref closure) = self.closure {
            Some(
                self.window
                    .request_animation_frame(closure.as_ref().unchecked_ref())
                    .expect("cannot set animation frame"),
            )
        } else {
            None
        }
    }

    pub fn is_paused(&self) -> bool {
        self.animation_id.is_none()
    }

    pub fn play(&mut self) -> Result<(), JsValue> {
        (self.playpause_button.as_ref() as &web_sys::Node).set_text_content(Some("⏸"));
        self.render_loop(0.0);
        Ok(())
    }

    pub fn pause(&mut self) -> Result<(), JsValue> {
        (self.playpause_button.as_ref() as &web_sys::Node).set_text_content(Some("▶"));
        if let Some(id) = self.animation_id {
            self.window.cancel_animation_frame(id)?;
            self.animation_id = None;
        }
        Ok(())
    }

    pub fn play_pause(&mut self) -> Result<(), JsValue> {
        if self.is_paused() {
            self.play()?;
        } else {
            self.pause()?;
        }
        Ok(())
    }
}
