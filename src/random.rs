/** non-cryptographic "fun" randomness functions */

pub fn choice<T>(arr: &[T]) -> &T {
    &arr[(js_sys::Math::random() * arr.len() as f64) as usize]
}

pub fn u32(max: u32) -> u32 {
    (js_sys::Math::random() * (max as f64)) as u32
}

pub fn bool() -> bool {
    js_sys::Math::random() >= 0.5
}
