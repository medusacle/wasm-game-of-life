extern crate cfg_if;
extern crate wasm_bindgen;
extern crate web_sys;

mod random;
mod renderer;
mod renderloop;
mod universe;
mod utils;

use std::cell::RefCell;
use std::cmp;
use std::rc::Rc;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
// use web_sys::console;

use renderer::{Renderer, CELL_SIZE};
use renderloop::RenderLoop;
use universe::Universe;

#[wasm_bindgen]
/** Interface handle to event loop, from javascript, also provides for memory management
 * by keeping references to structures.
 */
pub struct RenderLoopHandle {
    /** handle to inner render loop state structure */
    #[allow(dead_code)]
    render_loop: Rc<RefCell<RenderLoop>>,
    /** holds on to the closures until they can be dropped */
    #[allow(dead_code)]
    closures: Vec<Box<dyn Drop>>,
}

#[wasm_bindgen]
pub fn main(
    canvas: web_sys::HtmlCanvasElement,
    playpause_button: web_sys::HtmlElement,
    randomize_button: web_sys::EventTarget,
    omnicide_button: web_sys::EventTarget,
) -> Result<RenderLoopHandle, JsValue> {
    let window = web_sys::window().expect("no global `window` exists");
    let universe: Rc<RefCell<Universe>> = Rc::new(RefCell::new(Universe::new()));
    let renderer: Rc<RefCell<Renderer>> = Rc::new(RefCell::new(Renderer::new(
        canvas.clone(),
        universe.clone(),
    )?));
    // Holds on to the closures until they can be dropped
    let mut closures: Vec<Box<dyn Drop>> = Vec::new();
    // Mouse click handler on canvas
    {
        let closure: Closure<dyn Fn(_)> = {
            let canvas = canvas.clone();
            let universe = universe.clone();
            let renderer = renderer.clone();
            Closure::wrap(Box::new(move |event: web_sys::MouseEvent| {
                {
                    let mut universe = universe.borrow_mut();

                    let bounding_rect =
                        (canvas.as_ref() as &web_sys::Element).get_bounding_client_rect();
                    let scale_x = canvas.width() as f64 / bounding_rect.width();
                    let scale_y = canvas.height() as f64 / bounding_rect.height();
                    let canvas_left = (event.client_x() as f64 - bounding_rect.x()) * scale_x;
                    let canvas_top = (event.client_y() as f64 - bounding_rect.y()) * scale_y;
                    let cellsz = CELL_SIZE as f64;

                    let row = cmp::min((canvas_top / (cellsz + 1.0)) as u32, universe.height() - 1);
                    let col = cmp::min((canvas_left / (cellsz + 1.0)) as u32, universe.width() - 1);

                    universe.toggle_cell(row, col);
                }
                renderer.borrow().draw();
            }))
        };
        (canvas.as_ref() as &web_sys::EventTarget)
            .add_event_listener_with_callback("click", closure.as_ref().unchecked_ref())?;
        closures.push(Box::new(closure));
    }
    // Handle "randomize" button
    {
        let universe = universe.clone();
        let renderer = renderer.clone();
        let closure: Closure<dyn Fn(_) -> _> = Closure::wrap(Box::new(
            move |_event: web_sys::MouseEvent| -> Result<(), JsValue> {
                randomize_title()?;
                universe.borrow_mut().randomize();
                renderer.borrow().draw();
                Ok(())
            },
        ));
        randomize_button
            .add_event_listener_with_callback("click", closure.as_ref().unchecked_ref())?;
        closures.push(Box::new(closure));
    }
    // Handle "omnicide" button
    {
        let universe = universe.clone();
        let renderer = renderer.clone();
        let closure: Closure<dyn Fn(_)> =
            Closure::wrap(Box::new(move |_event: web_sys::MouseEvent| {
                universe.borrow_mut().kill_all();
                renderer.borrow().draw();
            }));
        omnicide_button
            .add_event_listener_with_callback("click", closure.as_ref().unchecked_ref())?;
        closures.push(Box::new(closure));
    }

    // Render loop handling
    let render_loop: Rc<RefCell<RenderLoop>> = Rc::new(RefCell::new(RenderLoop::new(
        window.clone(),
        playpause_button.clone(),
        universe.clone(),
        renderer,
    )));
    render_loop.borrow_mut().closure = Some({
        let render_loop = render_loop.clone();
        Closure::wrap(Box::new(move |time: f64| {
            render_loop.borrow_mut().render_loop(time);
        }))
    });

    // Play/pause button
    {
        let closure: Closure<dyn Fn() -> _> = {
            let render_loop = render_loop.clone();
            Closure::wrap(Box::new(move || -> Result<(), JsValue> {
                render_loop.borrow_mut().play_pause()?;
                Ok(())
            }))
        };
        (playpause_button.as_ref() as &web_sys::EventTarget)
            .add_event_listener_with_callback("click", closure.as_ref().unchecked_ref())?;
        closures.push(Box::new(closure));
    }

    randomize_title()?;
    render_loop.borrow_mut().play()?;

    Ok(RenderLoopHandle {
        render_loop,
        closures,
    })
}

/* TODO: mass toggling using mouseup, mousemove, mousedown */

/*** MISC ***/
const TITLE_HEAD: &[&str] = &[
    "alpha", "beta", "gamma", "delta", "omega", "aleph", "nabla", "eschaton",
];

pub fn randomize_title() -> Result<(), JsValue> {
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");

    document.set_title(&format!(
        "{}-{:04x}-{:04x}",
        random::choice(TITLE_HEAD),
        random::u32(65536),
        random::u32(65536)
    ));

    Ok(())
}
