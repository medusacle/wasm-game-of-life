/** RENDERING */
use std::cell::RefCell;
use std::rc::Rc;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

use crate::universe::{UCell, Universe};

pub struct Renderer {
    #[allow(dead_code)]
    canvas: web_sys::HtmlCanvasElement,
    ctx: web_sys::CanvasRenderingContext2d,
    universe: Rc<RefCell<Universe>>,
}

pub const CELL_SIZE: u32 = 5; // px
const GRID_COLOR: &str = "#202020";
const DEAD_COLOR: &str = "#000000";
const ALIVE_COLOR: &str = "#8000FF";

impl Renderer {
    pub fn new(
        canvas: web_sys::HtmlCanvasElement,
        universe: Rc<RefCell<Universe>>,
    ) -> Result<Renderer, JsValue> {
        {
            let universe = universe.borrow();
            canvas.set_height((CELL_SIZE + 1) * universe.height() + 1);
            canvas.set_width((CELL_SIZE + 1) * universe.width() + 1);
        }

        let ctx = canvas
            .get_context("2d")?
            .expect("Canvas must have a 2d context")
            .dyn_into::<web_sys::CanvasRenderingContext2d>()?;
        Ok(Renderer {
            canvas: canvas,
            universe: universe,
            ctx: ctx,
        })
    }

    fn draw_grid(&self) {
        let ctx = &self.ctx;
        let universe = self.universe.borrow();
        let width = universe.width();
        let height = universe.height();
        let cellsz = CELL_SIZE as f64;

        ctx.begin_path();
        ctx.set_stroke_style(&JsValue::from(GRID_COLOR));

        // Vertical lines.
        for i in 0..=width {
            ctx.move_to(i as f64 * (cellsz + 1.0) + 1.0, 0.0);
            ctx.line_to(
                i as f64 * (cellsz + 1.0) + 1.0,
                (cellsz + 1.0) * height as f64 + 1.0,
            );
        }

        // Horizontal lines.
        for j in 0..=height {
            ctx.move_to(0.0, j as f64 * (cellsz + 1.0) + 1.0);
            ctx.line_to(
                (cellsz + 1.0) * width as f64 + 1.0,
                j as f64 * (cellsz + 1.0) + 1.0,
            );
        }

        ctx.stroke();
    }

    fn draw_cells(&self) {
        let ctx = &self.ctx;
        let universe = self.universe.borrow();
        let cellsz = CELL_SIZE as f64;

        ctx.begin_path();

        for (status, style) in &[
            (UCell::Dead, JsValue::from(DEAD_COLOR)),
            (UCell::Alive, JsValue::from(ALIVE_COLOR)),
        ] {
            ctx.set_fill_style(style);
            for row in 0..universe.height() {
                for col in 0..universe.width() {
                    if universe.get_cell(row, col) == *status {
                        ctx.fill_rect(
                            col as f64 * (cellsz + 1.0) + 1.0,
                            row as f64 * (cellsz + 1.0) + 1.0,
                            cellsz,
                            cellsz,
                        );
                    }
                }
            }
        }

        ctx.stroke();
    }

    pub fn draw(&self) {
        self.draw_grid();
        self.draw_cells();
    }
}
