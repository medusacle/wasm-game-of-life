#!/usr/bin/env python3
'''Extrememly basic webserver, which sends wasm content type properly.'''
# based on https://gist.github.com/HaiyangXu/ec88cbdce3cdbac7b8d5
import http.server
from http.server import HTTPServer, BaseHTTPRequestHandler
import socketserver

PORT = 8080

Handler = http.server.SimpleHTTPRequestHandler

Handler.extensions_map = {
    '.manifest': 'text/cache-manifest',
    '.html': 'text/html',
    '.png': 'image/png',
    '.jpg': 'image/jpg',
    '.svg': 'image/svg+xml',
    '.css': 'text/css',
    '.js': 'application/x-javascript',
    '.wasm': 'application/wasm',
    '': 'application/octet-stream', # Default
}

class TCPServer(socketserver.TCPServer):
    allow_reuse_address=True
    def __init__(self):
        super().__init__(("", PORT), Handler)

httpd = TCPServer()
print("serving at port", PORT)
httpd.serve_forever()
